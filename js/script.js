

$(document).ready(function() {
   resizer();
   
   
   $(window).resize(function(){
      resizer();
   });

   try {
      $(window).bind('orientationchange', function () {
         resizer();
      });
   }catch(e){}



   $('.style-input').focus(function(){
         var def = $(this).attr("defvalue"),
            val = $(this).val();
         if (def===val){
            $(this).val('');

         }
         $(this).addClass('with-val');
      });

   $('.style-input').blur(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.phone-in').keydown(function(ev){
      if (ev.keyCode!==9 && (ev.keyCode<48 || ev.keyCode>57))
         if (ev.keyCode===8 || ev.keyCode===46 || ev.keyCode===37 || ev.keyCode===39 || ev.keyCode===36 || ev.keyCode===35 || ev.keyCode===107 || ev.keyCode===39){} else
            return false;
   });



   $('.button').click(function(){
      $(this).closest("form").submit();
   });

   $(window).scroll(function(){
      onScroll();
   });

   onScroll();
   
   setInterval(function(){
      var a = $('.so1.active'),
         b = a.next('.so1');
      if (b.length===0)
         b = a.prev('.so1');
      a.removeClass("active");
      b.addClass("active");
      a.stop().animate({opacity:0}, 2000);
      b.stop().animate({opacity:1}, 2000);
      $('.name').text(b.attr("toname"));
   }, 10000);

});

function onScroll(){
   var st = $('body').scrollTop()||$(window).scrollTop();
   var f = $('.ff')
   if (st>f.offset().top+f.height()){
      $('.but-top').css({"position":"fixed"});
   } else {
      $('.but-top').css({"position":"absolute"});
   }
}


function resizer(){
   setTimeout(function(){
      
      var w = $('body').width(),
         h = $(document).height();
      
      $('.right-car-bg').width(w+"px");


      if (w<1200){
         if (w<980){
            $('body').addClass('adaptive980');
            $('body').removeClass('adaptive1024');
            if (w<720){
               $('body').addClass('adaptive720');
               resetMargin(3);
            } else {
               resetMargin(2);
            }
         } else {
            $('body').addClass('adaptive1024');
            $('body').removeClass('adaptive980');
            $('body').removeClass('adaptive720');
            calcMargin();
         }
      } else {
         $('body').removeClass('adaptive1024');
         $('body').removeClass('adaptive720');
         resetMargin();
      }
   }, 1);
}

function calcMargin(){
   var w1 = $('.stikers').width(),
      mar = (w1-900)/2;

   if (mar<0 || mar>22){
      return resetMargin();
   }
   $('.stiker, .blue-stiker').css({"margin-right":mar+"px"});
   $('.stiker').eq(2).css({"margin-right":"0px"});
   $('.blue-stiker').eq(2).css({"margin-right":"0px"});
}

function resetMargin(type){
   $('.stiker, .blue-stiker').css({"margin-right": "22px"});
   $('.stiker').eq(2).css({"margin-right": "0px"});
   $('.blue-stiker').eq(2).css({"margin-right": "0px"});
   if (type==2){

      $('.stiker').eq(1).css({"margin-right": "0px"});
      $('.blue-stiker').eq(1).css({"margin-right": "0px"});
   } else if (type==3) {
      $('.stiker, .blue-stiker').css({"margin-right": "22px", "margin-left":"22px"});
   }


}